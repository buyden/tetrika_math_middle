# Задание 7
# Числовые неравенства, координатная прямая
## Дополнительный контент
Много примеров <https://oge.sdamgia.ru/test?likes=205776>  
Контент <https://vpr-ege.ru/oge/matematika/958-zadanie-7-oge-po-matematike-neravenstva>  
Примеры <https://4ege.ru/trening-gia-matematika/66002-zadanie-7-oge-po-matematike-praktika.html>  

## Основная часть теория+задания
![](Task7_content/701.png)
> Видео теории <https://youtu.be/qx2M6fsDDXs>

![](Task7_content/702.png)
![](Task7_content/703.png)
![](Task7_content/704.png)
![](Task7_content/707.png)
![](Task7_content/708.png)
![](Task7_content/709.png)
>Видео теории <https://youtu.be/yu6YxCSAeSM>

![](Task7_content/710.png)
![](Task7_content/711.png)
![](Task7_content/712.png)
![](Task7_content/713.png)
![](Task7_content/714.png)
![](Task7_content/715.png)
![](Task7_content/716.png)
> Видео теории <https://youtu.be/UzqSbz0RYT8>

![](Task7_content/717.png)
![](Task7_content/718.png)
![](Task7_content/719.png)
> Видео теории <https://youtu.be/R03_l9ctcFU>

![](Task7_content/720.png)
![](Task7_content/721.png)
![](Task7_content/722.png)
![](Task7_content/723.png)


