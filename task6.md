# Задание 6
# Вычисления и числа
## Дополнительный контент
![](Task6_content/Шпаргалка_6_задание__Ч1.png)
![](Task6_content/Шпаргалка_6_задание__Ч2.png)
![](Task6_content/Таблица квадратов.jpeg)
![](Task6_content/признаки_делимости.png)
![](Task6_content/свойства_степеней.png)


Много примеров https://math-oge.sdamgia.ru/test?id=49832466&nt=True&pub=False

## Основная часть теория+задания
![](Task6_content/6_1.png)
> Теория: Действия с обыкновенными дробями <https://youtu.be/TDtvCy2BA-A>

![](Task6_content/6_2.png)
![](Task6_content/6_3.png)
![](Task6_content/6_4.png)
![](Task6_content/6_5.png)

![](Task6_content/6_6.png)
> Теория: Действия с десятичными дробями <https://youtu.be/bb3dkRpRBio>

![](Task6_content/6_7.png)
![](Task6_content/6_8.png)
![](Task6_content/6_9.png)
![](Task6_content/6_10.png)

![](Task6_content/6_11.png)
> Теория: Действия с обыкновенными и десятичными дробями <https://youtu.be/KN4O21yNpZY>

![](Task6_content/6_12.png)
![](Task6_content/6_13.png)
![](Task6_content/6_14.png)
> Теория: Степени <https://youtu.be/lg_RJXJHZxg>

![](Task6_content/6_15.png)
![](Task6_content/6_16.png)
![](Task6_content/6_17.png)
![](Task6_content/6_18.png)
![](Task6_content/6_19.png)
